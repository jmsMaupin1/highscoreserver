const http = require('http');
const textBody = require('body');
const jsonBody = require('body/json');

const hostname = '127.0.0.1';
const port = 3000;

const scores = [
    {
        name: "Edwin",
        score: 50
    },
    {
        name: "David",
        score: 39
    }
]

const resources = {
    "/scores" : scores
}

function addAndSortScores(score) {
    // add new score to the scores list, sort them by score value then keep only top 3
    scores.push(score);
    scores.sort( (a, b) => b.score - a.score)
    scores.splice(3, scores.length - 1)
}

const server = http.createServer((req, res) => {
    if(resources[req.url] === undefined) {
        res.statusCode = 404;
        res.end('Wrong URL bruh')
    }
    if(req.method === "GET") { 
        res.statusCode = 200;
        res.setHeader('content-type', 'application/json');
        res.end(JSON.stringify(scores))
    } else if (req.method === "POST") {
        jsonBody(req, res, (err, body) => {
            res.statusCode = 201;
            addAndSortScores(body);
        //    scores.push(body);
            res.end(JSON.stringify(scores));
       })
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});